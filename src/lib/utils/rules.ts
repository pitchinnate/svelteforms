import {isNumber} from "lodash";

export const email = (val: string): string | undefined => {
    if (val === undefined || val === null || !val.includes('@')) return 'Not a valid email address';
}
export const required = (val: string): string | undefined => {
    if (val === undefined || val === null || val.trim() === '') return 'Required';
}
export const password = (val: string): string | undefined => {
    if (val === undefined || val === null || val.trim().length < 8) return 'Minimum password length is 8';
}
export const date = (val: string): string | undefined => {
    if (val && !Date.parse(val)) return 'Not a valid date';
};
export const number = (val: string): string | undefined => {
    if (val && !isNumber(val)) return 'Not a number';
}