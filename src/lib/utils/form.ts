export type InputRule = ((val: any) => string|undefined )
export type FormInput = {[key: string]: FormFieldInput};
export type FormOutput = {[key: string]: FormField};
export type FormData = {[key: string]: any};

export interface FormFieldInput {
    default?: any;
    rules?: InputRule[];
}

export class FormField {
    key: string;
    value: any;
    rules: InputRule[];
    valid = false;
    errors: string[] = [];
    displayErrors = '';
    files: any[] = [];

    public constructor(key: string, config: FormFieldInput) {
        this.key = key;
        this.value = config.default;
        this.rules = config.rules || [];
        this.valid = (this.rules.length === 0);
    }

    setDisplayErrors() {
        this.displayErrors = this.errors.join(' | ');
    }
}

export default class Form {
    form: FormOutput = {};

    public constructor(data: FormInput) {
        Object.keys(data).forEach((key: string) => {
            this.form[key] = new FormField(key, data[key]);
        });
    }

    public getData(): FormData {
        const data: FormData = {};
        Object.keys(this.form).forEach((key: string) => {
            data[key] = this.form[key].value;
        });
        return data;
    }

    public setData(values: FormData) {
        Object.keys(values).forEach((key: string) => {
            if (this.form[key]) {
                this.form[key].value = values[key];
            }
        });
    }

    public isValid(): boolean {
        let isValid = true;
        Object.keys(this.form).forEach((key: string) => {
            const invalidRules: string[] = [];
            this.form[key].rules.forEach((rule) => {
                const val =  rule(this.form[key].value);
                if (val) {
                    invalidRules.push(val);
                }
            });
            this.form[key].errors = invalidRules;
            this.form[key].setDisplayErrors();

            if (invalidRules.length > 0) {
                isValid = false;
            }
        });
        return isValid;
    }
}